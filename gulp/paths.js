/**
 * @author    Junxiang Wei {@link http://www.nodeunify.com}
 * @copyright Copyright (c) 2015, Junxiang Wei
 * @license   MIT
 */
'use strict';

import path from 'path';

const root = path.dirname(__dirname);
const paths = {
  root: root,
  /**
   * The 'gulpfile' file is where our run tasks are hold.
   */
  gulpfile: [`${root}/gulpfile.js`, `${root}/gulp/**/*.js`],
  jspm: `${root}/jspm_packages`,
  app: {
    basePath: `${root}/src/`,
    // config: {
    //   dev: `${root}/src/app/core/config/core.config.dev.js`,
    //   test: `${root}/src/app/core/config/core.config.test.js`,
    //   prod: `${root}/src/app/core/config/core.config.prod.js`
    // },


    scripts: [`${root}/src/app/**/*.js`],
    html: `${root}/src/index.html`,
    templates: `${root}/src/app/**/*.html`,
    json: `${root}/src/app/**/*.json`
  },
  /**
   * This is a collection of file patterns that refer to our app code (the
   * stuff in `src/`). These file paths are used in the configuration of
   * build tasks.
   *
   * - 'styles'       contains all project css styles
   * - 'images'       contains all project images
   * - 'fonts'        contains all project fonts
   * - 'scripts'      contains all project javascript except config-env.js and unit test files
   * - 'html'         contains main html files
   * - 'templates'    contains all project html templates
   * - 'config'       contains Angular app config files
   */
  appData: {
    basePath: `${root}/src/app-data/`,
    js: `${root}/src/app-data/js/`,
    fonts: [
      `${root}/src/app-data/fonts/**/*.{eot,svg,ttf,woff,woff2}`,
      `${root}/jspm_packages/**/*.{eot,svg,ttf,woff,woff2}`
    ],
    styles: `${root}/src/app-data/styles/**/*.scss`,
    images: `${root}/src/app-data/images/**/*.{png,gif,jpg,jpeg}`
  },
  /**
   * This is a collection of file patterns that refer to our app unit and e2e tests code.
   *
   * 'config'       contains karma and protractor config files
   * 'testReports'  contains unit and e2e test reports
   * 'unit'         contains all project unit test code
   * 'e2e'          contains all project e2e test code
   */
  // test: {
  //   basePath: `${root}/test/`,
  //   config: {
  //     karma: `${root}/karma.conf.js`,
  //     protractor: `${root}/protractor.conf.js`
  //   },
  //   testReports: {
  //     basePath: `${root}/test-reports/`,
  //     coverage: `${root}/test-reports/coverage/`
  //   },
  //   platoReports: `${root}/test/plato`,
  //   mock: `${root}/src/app/**/*.mock.js`,
  //   unit: `${root}/src/app/**/*.spec.js`,
  //   e2e: `${root}/test/e2e/**/*.e2e.js`
  // },


  /**
   * The 'tmp' folder is where our html templates are compiled to JavaScript during
   * the build process and then they are concatenating with all other js files and
   * copy to 'dist' folder.
   */
  tmp: {
    basePath: `${root}/.tmp/`,
    styles: `${root}/.tmp/styles/`,
    scripts: `${root}/.tmp/scripts/`
  },
  /**
   * The 'build' folder is where our app resides once it's
   * completely built.
   *
   * - 'dist'         application distribution source code
   * - 'docs'         application documentation
   */
  build: {
    dist: {
      basePath: `${root}/dist/`,
      fonts: `${root}/dist/fonts/`,
      images: `${root}/dist/images/`,
      styles: `${root}/dist/styles/`,
      scripts: `${root}/dist/scripts/`
    },
    docs: `${root}/docs/`
  }
};

export default paths;
