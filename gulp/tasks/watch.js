/**
 * @author    Junxiang Wei {@link http://www.nodeunify.com}
 * @copyright Copyright (c) 2015, Junxiang Wei
 * @license   MIT
 */
'use strict';

import gulp from 'gulp';
import historyApiFallback from 'connect-history-api-fallback';
import browserSync from 'browser-sync';
import paths from '../paths';

/**
 * The 'watch' task set up the checks to see if any of the files listed below
 * change, and then to execute the listed tasks when they do.
 */
gulp.task('watch', () => {
  browserSync.create();

  gulp.watch([paths.jspm], [browserSync.reload]);
  gulp.watch([paths.app.basePath, paths.gulpfile], [browserSync.reload]);

  browserSync.init({
    server: {
      baseDir: [paths.app.basePath, paths.jspm, paths.root],
      // For AngularJS html5mode
      // @see https://github.com/BrowserSync/browser-sync/issues/204#issuecomment-102623643
      // @sere https://github.com/bripkens/connect-history-api-fallback
      middleware: [ historyApiFallback() ]
    }
  });
});
