/**
 * @author    Junxiang Wei {@link http://www.nodeunify.com}
 * @copyright Copyright (c) 2015, Junxiang Wei
 * @license   MIT
 */
'use strict';

import fs from 'fs';
import path from 'path';
import gulp from 'gulp';
import eslint from 'gulp-eslint';
import jspm from 'jspm';
import paths from '../paths';

/**
 * The 'eslint' task defines the rules of our hinter as well as which files
 * we should check. It helps to detect errors and potential problems in our
 * JavaScript code.
 * TODO: .eslintrc doesn't work
 *
 * @return {Stream}
 */
gulp.task('eslint', () => {
  return gulp.src(`{paths.app.scripts}`)
    // eslint() attaches the lint output to the eslint property
    // of the file object so it can be used by other modules.
    .pipe(eslint())
    // eslint.format() outputs the lint results to the console.
    // Alternatively use eslint.formatEach() (see Docs).
    .pipe(eslint.format())
    // To have the process exit with an error code (1) on
    // lint error, return the stream and pipe to failOnError last.
    .pipe(eslint.failOnError());
});

/**
 * Create JS production bundle.
 */
gulp.task('bundle', ['eslint'], () => {
  const inputFile = 'src/app/bootstrap';
  const outputFile = `${paths.tmp.basePath}build.js`;
  const outputOptions = {
    sourceMaps: false
      // minify: true
  };
  // Ensure output directory exists
  const outputDir = path.dirname(outputFile);
  if (!fs.existsSync(outputDir)) {
    fs.mkdirSync(outputDir);
  }

  return jspm.bundleSFX(inputFile, outputFile, outputOptions);
});
