/**
 * @author    Junxiang Wei {@link http://www.nodeunify.com}
 * @copyright Copyright (c) 2015, Junxiang Wei
 * @license   MIT
 */
'use strict';

import gulp from 'gulp';
import historyApiFallback from 'connect-history-api-fallback';
import browserSync from 'browser-sync';
import paths from '../paths';

gulp.task('serve', ['build'], () => {
  browserSync.create();
  browserSync.init({
    server: {
      baseDir: [paths.build.dist.basePath],
      middleware: [ historyApiFallback() ]
    }
  });
});
