/**
 * @author    Junxiang Wei {@link http://www.nodeunify.com}
 * @copyright Copyright (c) 2015, Junxiang Wei
 * @license   MIT
 */
'use strict';

import del from 'del';
import gulp from 'gulp';
import inject from 'gulp-inject';
import usemin from 'gulp-usemin';
// TODO: use uglify for scripts
// import uglify from 'gulp-uglify';
import runSequence from 'run-sequence';
import paths from '../paths';

/**
 * The 'clean' task delete 'build' and '.tmp' directories.
 *
 * @param {Function} done - callback when complete
 */
gulp.task('clean', (cb) => {
  const files = [].concat(paths.build.dist.basePath, paths.tmp.basePath);
  return del(files, cb);
});

/**
 * The 'copy' task just copies files from A to B. We use it here
 * to copy our files that haven't been copied by other tasks
 * e.g. (favicon, etc.) into the `build/dist` directory.
 *
 * @return {Stream}
 */
gulp.task('extras', () => {
  return gulp.src([paths.app.basePath + '*.{ico,png,txt}'])
    .pipe(gulp.dest(paths.build.dist.basePath));
});

gulp.task('copy-data', () => {
  return gulp.src(['src/app-data/**/*'])
    .pipe(gulp.dest('dist/app-data'));
});

gulp.task('sitemap', () => {
  return gulp.src(`${paths.app.basePath}sitemap.xml`)
    .pipe(gulp.dest(paths.build.dist.basePath));
});

/**
 * The 'compile' task compile all js, css and html files.
 *
 * 1. it inject bundle into `index.html`
 * 2. css      - minify, add revision number, add banner header
 *     js         - annotates the sources before minifying, minify, add revision number, add banner header
 *     html   - minify, add banner header
 *
 * @return {Stream}
 */
gulp.task('compile', ['bundle'], () => {
  return gulp.src(paths.app.html)
    .pipe(inject(gulp.src(`${paths.tmp.basePath}build.css`, {
      read: false
    }), {
      starttag: '<!-- inject:build.css -->',
      addRootSlash: false
    }))
    .pipe(inject(gulp.src(`${paths.tmp.basePath}build.js`, {
      read: false
    }), {
      starttag: '<!-- inject:build.js -->',
      addRootSlash: false
    }))
    .pipe(usemin({
      css: [],
      js: [],
      html: []
    }))
    .pipe(gulp.dest(paths.build.dist.basePath));
});

/**
 * The 'build' task gets app ready for deployment by processing files
 * and put them into directory ready for production.
 *
 * @param {Function} done - callback when complete
 */
gulp.task('build', (cb) => {
  runSequence(['clean'], ['compile', 'copy-data', 'sitemap', 'fonts'], cb);
});
