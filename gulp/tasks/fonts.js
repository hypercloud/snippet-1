/**
 * @author    Junxiang Wei {@link http://www.nodeunify.com}
 * @copyright Copyright (c) 2015, Junxiang Wei
 * @license   MIT
 */
'use strict';

import gulp from 'gulp';
import flatten from 'gulp-flatten';
import paths from '../paths';

/**
 * The 'fonts' task copies fonts to `dist` directory.
 *
 * @return {Stream}
 */
gulp.task('fonts', () => {
  return gulp.src(paths.appData.fonts)
    .pipe(flatten())
    .pipe(gulp.dest(paths.build.dist.fonts));
});
