/**
 * @author    Junxiang Wei {@link http://www.nodeunify.com}
 * @copyright Copyright (c) 2015, Junxiang Wei
 * @license   MIT
 */
'use strict';

require('babel/register');
require('require-dir')('./gulp/tasks', {
  recurse: true
});
