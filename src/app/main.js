/**
 * @author    Junxiang Wei {@link http://www.nodeunify.com}
 * @copyright Copyright (c) 2015, Junxiang Wei
 * @license   MIT
 */
 /*eslint no-unused-vars: 0*/
'use strict';

import './vendor';

// core modules
import coreModule from './core/core';

// state modules
import pageModule from './states/page/page';

let mainModule = angular.module('app', [
    'ui.router',
    // core modules
    coreModule.name,
    // state modules
    pageModule.name
  ]);

export default mainModule;
