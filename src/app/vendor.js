/**
 * @author    Junxiang Wei {@link http://www.nodeunify.com}
 * @copyright Copyright (c) 2015, Junxiang Wei
 * @license   MIT
 */
/*eslint no-unused-vars: 0*/
'use strict';

// js vendor files
import $ from 'jquery';
import 'bootstrap';

import 'angular';
import 'angular-ui-router';

// css vendor files
import 'bootstrap/css/bootstrap.min.css!';
