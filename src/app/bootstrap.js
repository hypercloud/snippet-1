/**
 * @author    Junxiang Wei {@link http://www.nodeunify.com}
 * @copyright Copyright (c) 2015, Junxiang Wei
 * @license   MIT
 */
'use strict';

import mainModule from './main';

angular.element(document).ready(() => {
  angular.bootstrap(document, [mainModule.name], {});
});
