/**
 * @author    Junxiang Wei {@link http://www.nodeunify.com}
 * @copyright Copyright (c) 2015, Junxiang Wei
 * @license   MIT
 */
'use strict';

export function onConfig($urlRouterProvider, $locationProvider) {
  'ngInject';
  // Router configuration
  $urlRouterProvider.otherwise('/');

  $locationProvider.html5Mode(true);
}
