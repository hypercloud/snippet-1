/**
 * @author    Junxiang Wei {@link http://www.nodeunify.com}
 * @copyright Copyright (c) 2015, Junxiang Wei
 * @license   MIT
 */
'use strict';

import { onConfig } from './config/config';

export default angular.module('app.core', [])
  .config(onConfig);
