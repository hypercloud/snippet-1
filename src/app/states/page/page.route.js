/**
 * @author    Junxiang Wei {@link http://www.nodeunify.com}
 * @copyright Copyright (c) 2015, Junxiang Wei
 * @license   MIT
 */
'use strict';

import pageTemplate from './page.html!text';
import productPageTemplate from './page.product.html!text';
import addressPageTemplate from './page.address.html!text';
import paymentPageTemplate from './page.payment.html!text';

function pageRoute($stateProvider) {
  'ngInject';
  $stateProvider
    .state('page', {
      url: '/page',
      template: pageTemplate,
      controller: 'PageCtrl'
    })
    .state('page.product', {
      url: '/product',
      template: productPageTemplate,
      controller: 'ProductPageCtrl'
    })
    .state('page.address', {
      url: '/address',
      template: addressPageTemplate,
      controller: 'AddressPageCtrl'
    })
    .state('page.payment', {
      url: '/payment',
      template: paymentPageTemplate,
      controller: 'PaymentPageCtrl'
    })
}

export default pageRoute;
