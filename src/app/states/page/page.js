/**
 * @author    Junxiang Wei {@link http://www.nodeunify.com}
 * @copyright Copyright (c) 2015, Junxiang Wei
 * @license   MIT
 */
'use strict';

import pageRoute from './page.route';

import PageCtrl from './page.controller';
import ProductPageCtrl from './page.product.controller';
import AddressPageCtrl from './page.address.controller';
import PaymentPageCtrl from './page.payment.controller';

export default angular.module('app.page', [
  ])
  .config(pageRoute)
  .controller('PageCtrl', PageCtrl)
  .controller('ProductPageCtrl', ProductPageCtrl)
  .controller('AddressPageCtrl', AddressPageCtrl)
  .controller('PaymentPageCtrl', PaymentPageCtrl);
